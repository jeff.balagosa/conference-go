from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


# Defining a custom JSONEncoder for QuerySet objects
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


# Defining a custom JSONEncoder for datetime objects
class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


# Defining a custom JSONEncoder for Django model objects
class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # A dictionary of custom encoders for model properties
    encoders = {}

    def default(self, o):
        # If the object is an instance of the model, encode it
        if isinstance(o, self.model):
            dict = {}
            # If the model has a get_api_url method, add its return value to the dictionary
            if hasattr(o, "get_api_url"):
                dict["href"] = o.get_api_url()
            # For each property of the model, add its value to the dictionary
            for property in self.properties:
                value = getattr(o, property)
                # If a custom encoder is defined for the property, use it to encode the value
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                dict[property] = value
            # Add any extra data to the dictionary
            dict.update(self.get_extra_data(o))
            return dict
        else:
            return super().default(o)

    # A method to get any extra data to be added to the dictionary for a model object
    def get_extra_data(self, o):
        return {}
