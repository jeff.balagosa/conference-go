import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Set the headers for the API request.
    headers = {"Authorization": PEXELS_API_KEY}
    # Set the parameters for the API request.
    params = {"per_page": 1, "query": city + " " + state}
    # Set the URL for the API request.
    url = "https://api.pexels.com/v1/search"
    # Send the API request and store the response.
    response = requests.get(url, params=params, headers=headers)
    # Parse the response content as JSON.
    content = json.loads(response.content)
    try:
        # Return a dictionary containing the URL for the first photo in the response.
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        # If there is an error, return a dictionary with a None value for the URL.
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},ISO 3166-2&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    geo_response = requests.get(geo_url)

    # Parse the JSON response
    loc = json.loads(geo_response.content)

    # Get the latitude and longitude from the response
    lat = loc[0]["lat"]
    lon = loc[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    curr_weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # Make the request
    weather_response = requests.get(curr_weather_url)
    # Parse the JSON response
    parsed_weather = json.loads(weather_response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    weather_dict = {
        "temp": parsed_weather["main"]["temp"],
        "description": parsed_weather["weather"][0]["description"],
    }
    # Return the dictionary
    try:
        return weather_dict
    except KeyError:
        return None
